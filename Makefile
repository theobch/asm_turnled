C	= gcc
NAME	= turnled
SRCS	= turnled.s
OBJS	= $(SRCS:.s=.o)

PRINTF	= printf
RED	= "\e[0;31m"
BLUE	= "\e[0;34m"
GREEN	= "\e[0;32m"
DEFAULT = "\e[0m"

all:	$(NAME)

$(NAME): $(OBJS)
	@$(CC) $(OBJS) -o $(NAME) && \
	$(PRINTF) "%b" "$(NAME) : " $(GREEN)"ready\n"$(DEFAULT)

%.o : %.s
	@$(CC) -c $< -o $@
	@$(PRINTF) "%b" "$< : " $(GREEN)"ok\n"$(DEFAULT) || \
	$(PRINTF) "%b" "$< : " $(RED)"error\n"$(DEFAULT)

clean:
	@rm -f $(OBJS) && \
	$(PRINTF) "clean ok\n"

fclean:	clean
	@rm -f $(NAME) && \
	$(PRINTF) "fclean done\n"

re: fclean all

.PHONY: all clean fclean re
