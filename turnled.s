     .data
int_format_string:  .asciz " %d"
next_line_string: .asciz "\n"
mem_file_string: .asciz "/dev/mem"
open_err_string: .asciz "open() error\n"
mmap_err_string: .asciz "mmap() error\n"
@ string for debug
debug_line1: .asciz "\nr0: 0x%08x, r1: 0x%08x, r2: 0x%08x\n"
debug_line2: .asciz "r3: 0x%08x, r4: 0x%08x, r5: 0x%08x\n"
debug_line3: .asciz "r6: 0x%08x, r7: 0x%08x, r8: 0x%08x\n"
debug_line4: .asciz "r9: 0x%08x, r10: 0x%08x, r11: 0x%08x\n"
debug_line5: .asciz "r12: 0x%08x, r13: 0x%08x, r14: 0x%08x\n"

	.align
@ define constants
.equ SWITCH,    24
.equ LED,       18
.equ LOW,       0
.equ HIGH,      1
.equ GPIO_BASE, 0x3f200000
.equ GPIO_SIZE, 256
.equ GFPSEL0_OFFSET, 0x0
.equ GPSET0_OFFSET,  0x1C
.equ GPCLR0_OFFSET,  0x28
.equ GPLEV0_OFFSET,  0x34

.equ O_RDWR, 2
.equ O_SYNC, 1052672
.equ NULL, 0
.equ PROT_READ, 1
.equ PROT_WRITE, 2
.equ MAP_SHARED, 1
.equ MAP_FAILED, -1


	.text
	.global main
	.extern	printf
    .extern open
    .extern mmap
    .extern sleep

main:
    stmfd r13!, {r14}

    @ call open(): int fd = open("/dev/mem", O_RDWR | O_SYNC);
    ldr r0, =mem_file_string
    mov r1, #O_RDWR
    ldr r2, =#O_SYNC
    orr r1, r1, r2
    bl open
    @ r0 has the file descriptor

    cmp r0, #0
    blt open_error

    @ r7 has the file descriptor
    mov r7, r0

    @call mmap(): char* addr = mmap(NULL, GPIO_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, GPIO_BASE);
    mov r0, #NULL
    mov r1, #GPIO_SIZE
    mov r2, #PROT_READ
    orr r2, r2, #PROT_WRITE
    mov r3, #MAP_SHARED
    mov r4, r7
    ldr r5, =#GPIO_BASE
    stmfd r13!, {r4-r5}
    bl mmap
    ldmfd r13!, {r4-r5}

    @ r0 has the address of mapped region
    cmp r0, #MAP_FAILED
    beq mmap_error

    @ now r10 has the address of mapped region
    mov r10, r0

    @ set_gpio_input: void set_gpio_input(int gpio_no, char *mmap_addr);
    mov r0, #SWITCH
    mov r1, r10
    bl set_gpio_input

    @ set_gpio_output: void set_gpio_output(int gpio_no, char *mmap_addr);
    mov r0, #LED
    mov r1, r10
    bl set_gpio_output
    
    @ r5 is a counter for the loop
    mov r5, #5
while_loop:
    cmp r5, #0
    beq exit

    mov r0, #SWITCH
    mov r1, r10
    bl gpio_read
    @ r0 has the output result
    cmp r0, #LOW
    bne while_loop

    @ turn on the LED
    mov r0, #LED
    mov r1, r10
    bl gpio_write

    mov r0, #1
    bl sleep

    @ turn off the LED
    mov r0, #LED
    mov r1, r10
    bl gpio_clear

    sub r5, r5, #1
    b while_loop

open_error:
    ldr r0, =open_err_string
    bl printf
    b exit

mmap_error:
    ldr r0, =mmap_err_string
    bl printf
    b exit

exit:
    ldmfd r13!, {pc}

@ void set_gpio_input(int gpio_no, char *mmap_addr);
@ [input] r0: gpio_no, r1: *mmap_addr
set_gpio_input:
    stmfd r13!, {r0-r12, r14}

    @ move gpio_no to r6
    mov r6, r0

    @ r5 is for the GPIO function select register
    @ calculate mmap_addr + GFPSEL0_OFFSET + (gpio_no / 10 * 4)
    add r5, r1, #GFPSEL0_OFFSET

    @ move gpio_no to r6
    mov r0, r6
    mov r1, #10
    bl divide
    @ r0 has the division result
    mov r1, #4
    mul r2, r0, r1
    add r5, r5, r2

    @ calculate: shift_amount = (gpio_no % 10) * 3;
    mov r0, r6
    mov r1, #10
    bl remainder
    @ r0 has the remainder result
    mov r8, #3
    mul r7, r0, r8

    @ process: *reg = *reg & ~(0b111 << shift_amount);
    mov r8, #0b111
    mov r9, #0
    add r9, r9, r8, LSL r7
    mvn r9, r9
    ldr r10, [r5]
    and r10, r10, r9
    str r10, [r5]

    ldmfd r13!, {r0-r12, pc}

@ void set_gpio_output(int gpio_no, char *mmap_addr);
@ [input] r0: gpio_no, r1: *mmap_addr
set_gpio_output:
    stmfd r13!, {r0-r12, r14}

    @ move gpio_no to r6
    mov r6, r0

    @ r5 is for the GPIO function select register
    @ calculate mmap_addr + GFPSEL0_OFFSET + (gpio_no / 10 * 4)
    add r5, r1, #GFPSEL0_OFFSET

    @ move gpio_no to r6
    mov r0, r6
    mov r1, #10
    bl divide
    @ r0 has the division result
    mov r1, #4
    mul r2, r0, r1
    add r5, r5, r2

    @ calculate: shift_amount = (gpio_no % 10) * 3;
    mov r0, r6
    mov r1, #10
    bl remainder
    @ r0 has the remainder result
    mov r8, #3
    mul r7, r0, r8

    @ process: *reg = *reg | (1 << shift_amount);
    mov r8, #1
    mov r9, #0
    add r9, r9, r8, LSL r7

    ldr r10, [r5]
    orr r10, r10, r9
    str r10, [r5]

    ldmfd r13!, {r0-r12, pc}

@ int gpio_read(int gpio_no, char *mmap_addr);
@ [input] r0: gpio_no, r1: *mmap_addr
@ [output] r0: output
gpio_read:
    stmfd r13!, {r1-r12, r14}

    @ please implement your own code here

    @ Store 2nd argument + GPLEV0_OFFSET macro inside r3
    ldr	r3, [r1, #GPLEV0_OFFSET]

    @ Declare a second register with '1' as value 
    mov	r2, #1

    @ Return statement, with LSL representig the '<<' in the return statement
    and	r0, r3, r2, lsl r0

    ldmfd r13!, {r1-r12, pc}

@ void gpio_write(int gpio_no, char *mmap_addr)
@ [input] r0: gpio_no, r1: *mmap_addr
gpio_write:
    stmfd r13!, {r0-r12, r14}

    @ please implement your own code here

    @ Declare a register with '1' as value
    mov	r3, #1

    @ Set r0 as the value of '1' shifted by the second argument of the function 
    lsl	r0, r3, r0

    @ Store the result with GPSET0_OFFSET
    str	r0, [r1, #GPSET0_OFFSET]

    ldmfd r13!, {r0-r12, pc}


@ void gpio_clear(int gpio_no, char *mmap_addr)
@ [input] r0: gpio_no, r1: *mmap_addr
gpio_clear:
    stmfd r13!, {r0-r12, r14}

    @ please implement your own code here

    @ Declare a register with '1' as value
    mov r3, #1

    @ Set r0 as the value of '1' shifted by the second argument of the function 
    lsl	r0, r3, r0

    @ Store the result with GPCLR0_OFFSET
    str	r0, [r1, #GPCLR0_OFFSET]

    ldmfd r13!, {r0-r12, pc}

@ int divide(int number_to_divide, int divisor)
@ [input] r0: number_to_divide, r1:divisor
@ [output] r0: result
divide:
    stmfd r13!, {r2-r12, r14}
    mov r2, r1
    mov r1, r0
    mov r0, #0
    mov r3, #1    
 divide_start: 
    cmp r2, r1
    movls r2, r2, LSL #1
    movls r3, r3, LSL #1
    bls divide_start
divide_next:
    cmp r1, r2      
    subcs r1, r1, r2
    addcs r0, r0, r3 
    movs r3, r3, LSR #1     
    movcc r2, r2, LSR #1                        
    bcc divide_next
    ldmfd r13!, {r2-r12, pc}

@ int remainder(int number_to_divide, int divisor)
@ [input] r0: number_to_divide, r1:divisor
@ [output] r0: result
remainder:
    stmfd r13!, {r1-r12, r14}
    bl divide
    mov r0, r1
    ldmfd r13!, {r1-r12, pc}

debug:
    stmfd r13!, {r0-r12, r14}
    
    stmfd r13!, {r3}
    mov r3, r2
    mov r2, r1
    mov r1, r0
    ldr r0, =debug_line1
    bl printf
    ldmfd r13!, {r3}

    ldr r0, =debug_line2
    mov r1, r3
    mov r2, r4
    mov r3, r5
    bl printf

    ldr r0, =debug_line3
    mov r1, r6
    mov r2, r7
    mov r3, r8
    bl printf

    ldr r0, =debug_line4
    mov r1, r9
    mov r2, r10
    mov r3, r11
    bl printf

    ldr r0, =debug_line5
    mov r1, r12
    mov r2, r13
    mov r3, r14
    bl printf

    ldmfd r13!, {r0-r12, pc}
